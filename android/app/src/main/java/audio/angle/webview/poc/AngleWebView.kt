package audio.angle.webview.poc

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import android.webkit.WebView

class AngleWebView : WebView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onWindowVisibilityChanged(visibility: Int) {
        if (visibility != View.GONE) {
            super.onWindowVisibilityChanged(View.VISIBLE)
        }
    }


    fun init(activity: Activity) {
        settings.mediaPlaybackRequiresUserGesture = false
        settings.domStorageEnabled = true
        settings.javaScriptEnabled = true
        webChromeClient = object : WebChromeClient() {
            override fun onPermissionRequest(request: PermissionRequest?) {
                if (request == null) {
                    return
                }
                activity.runOnUiThread {
                    // TODO: only give mic permission!

                    val origin = request.origin.toString()
                    if (origin == "https://staging.app.angle.audio/" || origin == "https://app.angle.audio") {
                        request.grant(request.resources)
                    } else {
                        request.deny()
                    }
                }
            }
        }
    }


}