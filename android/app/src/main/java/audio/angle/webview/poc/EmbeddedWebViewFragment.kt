package audio.angle.webview.poc

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import audio.angle.webview.poc.databinding.FragmentEmbeddedWebviewBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class EmbeddedWebViewFragment : Fragment() {

    private var _binding: FragmentEmbeddedWebviewBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentEmbeddedWebviewBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val context = view.context
        requestPermissions(
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.INTERNET,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.BLUETOOTH
            ), 1
        )
        if (context is MainActivity) {
            binding.embeddedWebview.init(context)
            binding.embeddedWebview.loadUrl("https://staging.app.angle.audio")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}