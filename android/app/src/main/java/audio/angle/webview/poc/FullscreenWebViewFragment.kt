package audio.angle.webview.poc

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import audio.angle.webview.poc.databinding.FragmentFullscreenWebviewBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class FullscreenWebViewFragment : Fragment() {

    private var _binding: FragmentFullscreenWebviewBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFullscreenWebviewBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val context = view.context
        if (context is MainActivity) {
            binding.fullscreenWebview.init(context)
            binding.fullscreenWebview.loadUrl("https://staging.app.angle.audio")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}