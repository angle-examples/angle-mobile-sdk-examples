import AVFoundation
import SwiftUI

@main
struct AngleWebViewPOCApp: App {
    
    
    var body: some Scene {
        WindowGroup {
            ContentView().onAppear(perform: {
                AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                    if granted {
                        print("granted")
                    } else{
                        print("not granted")
                    }
                 })
            })
        }
    }
}
