import Foundation
import UIKit
import SwiftUI
import Combine
import WebKit

struct AngleWebView: UIViewRepresentable {
    
    var url: URL

    func makeUIView(context: Context) -> WKWebView {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.allowsInlineMediaPlayback = true
        
        let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
        webView.load(URLRequest(url: url))
       return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
    }
}
