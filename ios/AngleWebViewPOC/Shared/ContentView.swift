import SwiftUI

struct ContentView: View {
    var url = URL(string: "https://staging.app.angle.audio/join/a5793cee-4831-4fdf-b6c0-5ca9dd34a643?embedded=true&name=Example%20User")!
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Text("Angle in a WebView").font(.title).padding(.top, 20).padding(.bottom, 20)
                
                AngleWebView(url: url).overlay (
                    RoundedRectangle(cornerRadius: 4, style: .circular)
                        .stroke(Color.gray, lineWidth: 0.5)
                ).padding(EdgeInsets(
                top: 20, leading: 20, bottom: 20, trailing: 20))
            }
        }
    }
}
